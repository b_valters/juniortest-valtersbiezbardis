<?php
declare(strict_types=1);
include '../src/Db/Database.php';
include '../src/Lib/Router.php';
include '../src/Lib/Request.php';
include '../src/Lib/Response.php';
include '../src/Lib/Helpers.php';
include '../src/Model/FieldValue.php';
include '../src/Model/Item.php';
include '../src/Model/Field.php';
include '../src/Model/Fieldset.php';

use App\Db\Database;
use App\Lib\Request;
use App\Lib\Response;
use App\Lib\Router;
use App\Model\Fieldset;
use App\Model\FieldValue;
use App\Model\Item;

$database = new Database();
$errors = [];

Router::get('/product/list', function () {
    render('ProductList', ['items' => Item::all()]);
});
Router::get('/', function () {
    header('Location: /product/list');
});
Router::get('/product/add', function () {
    render('ProductAdd', ['fieldsets' => Fieldset::all()]);
});


Router::post('/product/delete', function (Request $request, Response $response) {
    Item::delete($request->data["ids"]);
});
Router::post('/product/create', function (Request $request, Response $response) {
    header('Location: /product/list');
    $item = new Item();
    $item->sku = $request->data["sku"];
    $item->name = $request->data["name"];
    $item->price = $request->data["price"];
    $lastAddedId = $item->save();
    $field_value = new FieldValue();
    $field_value->item_id = $lastAddedId;
    foreach ($request->data['fields'] as $fieldId => $value) {
        $field_value->field_id = $fieldId;
        $field_value->value = $value;
        $field_value->save();
    }
});

?>



