"use strict";
(function () {

    let lastIndex = -1;
    document.getElementById('type').addEventListener("change", function (e) {
        if (this.selectedIndex === 0 && lastIndex === -1) return;
        const c = document.getElementById("fieldsets").children;
        if (lastIndex !== -1) {
            c[lastIndex].classList.add('hidden');
        }
        if (this.selectedIndex === 0) return;


        c[this.selectedIndex - 1].classList.remove('hidden');
        lastIndex = this.selectedIndex - 1;
    });




    document.getElementById('form').addEventListener("submit", function (e) {
        e.preventDefault();
        const hiddenFields = document.getElementsByClassName("hidden");

        while (hiddenFields.length > 0) {
            fieldsets.removeChild(hiddenFields[0]);
        }

        this.submit();
    });

})();