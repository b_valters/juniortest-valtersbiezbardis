"use strict";
(function () {
    document.getElementById('delete-button').addEventListener("click", () => {
        const checkboxes = document.querySelectorAll("input:checked");

        let ids = [];
        for (let item of checkboxes) {
            ids.push(item.dataset.id);

        }

        const data = new FormData();
        data.append("ids", ids);
        fetch("/product/delete", {
            method: "POST",
            body: data,
        }).then(res => {
            location.reload();
        });

    });

     document.getElementById('add-button').addEventListener("click", () => {
         location.href = '/product/add';
     });
})();