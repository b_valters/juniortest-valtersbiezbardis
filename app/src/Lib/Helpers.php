<?php
function render($template, $vars = array())
{
    extract($vars);
    include "../src/Views/$template.php";
}
