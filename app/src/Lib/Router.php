<?php

namespace App\Lib;


class Router
{
    public static function get($route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'GET') !== 0) {
            return;
        }
        self::on($route, $callback);
    }

    public static function on($regex, $cb)
    {
        $parameters = $_SERVER['REQUEST_URI'];
        $parameters = (stripos($parameters, "/") !== 0) ? "/" . $parameters : $parameters;
        $regex = str_replace('/', '\/', $regex); // to escape the slashes for regex

        $is_match = preg_match('/^' . $regex . '$/', $parameters, $matches, PREG_OFFSET_CAPTURE);
        if ($is_match) {
            array_shift($matches);// we don't need the first element because it contains the whole uri string
            $parameters = array_map(function ($params) {
                return $params[0];
            }, $matches);

            $cb(new Request($parameters), new Response());
        }
    }

    // t

    public static function post($route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') !== 0) {
            return;
        }
        self::on($route, $callback);
    }

}