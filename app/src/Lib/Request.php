<?php

namespace App\Lib;

class Request
{
    public array $params;
    public array $data;

    public function __construct(array $params = [])
    {
        $this->params = $params;
        $this->data = $_POST;
    }


}