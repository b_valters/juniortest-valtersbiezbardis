<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Product Add</title>
    <link rel="stylesheet" href="../../public/css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body>
<header>
    <h1>Product Add</h1>
    <div>
        <button id="submit-button" type="submit" form="form">Save</button>
        <a href="/product/list" id="cancel-button">Cancel</a>
    </div>

</header>

<div class="input-form">
    <form id="form" action="/product/create" method="post">
        <div class="field">
            <label for="sku">SKU</label>
            <input type="text" id="sku" name="sku" required>
        </div>
        <div class="field">
            <label for="name">Name</label>
            <input type="text" id="name" name="name" required>
        </div>
        <div class="field">
            <label for="price">Price ($)</label>
            <input type="number" id="price" name="price" step="0.01" min="0" required>
        </div>

        <div class="field">
            <label for="type">Type Switcher</label>
            <select name="type" id="type" required>
                <option></option>
                <?php foreach ($fieldsets as $fieldset) { ?>
                    <option><?php echo $fieldset->type_name ?></option>
                <?php } ?>
            </select>

        </div>


        <div id="fieldsets">
            <?php foreach ($fieldsets as $fieldset) {
                ?>
                <div class="hidden">
                    <?php foreach ($fieldset->getFields() as $field) { ?>
                        <div class="field">
                            <label for="<?php echo $field->label ?>"><?php echo $field->label ?>
                                (<?php echo $field->unit ?>
                                )</label>
                            <input type="number" id="<?php echo $field->label ?>"
                                   name="fields[<?php echo $field->id ?>]">
                        </div>

                    <?php } ?>
                    <div class="field">
                        <p><?php echo $fieldset->description ?></p>
                    </div>

                </div>
            <?php } ?>
        </div>


    </form>
</div>


<footer>
    <P>Scandiweb Test assignment</P>
</footer>

<script src="../../public/js/scripts_productAdd.js"></script>
</body>
</html>


