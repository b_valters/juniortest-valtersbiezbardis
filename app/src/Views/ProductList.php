<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Product list</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body>
<header>
    <h1>Product List</h1>
    <div>
        <a href="/product/add" id="add-button">Add</a>
        <a href="" id="delete-button">Mass delete</a>
    </div>

</header>


<content>
    <?php foreach ($items as $item) {
        ?>
        <div class="item">
            <input name="checkbox" type="checkbox" data-id="<?php echo $item->id ?>">
            <div class="item-info">
                <p><?php echo $item->sku ?></p>
                <p><?php echo $item->name ?></p>
                <p><?php echo $item->price ?> $</p>
                <p><?php echo $item->printFormatted() ?></p>
            </div>
        </div>
    <?php } ?>
</content>



<footer>
    <P>Scandiweb Test assignment</P>
</footer>

<script src="js/script_productList.js"></script>
</body>
</html>