<?php

namespace App\Db;

use PDO;
use PDOException;

class Database
{
    private const servername = "db";
    private const username = "user";
    private const password = "password";
    private ?PDO $connection = null;

    function __construct()
    {
        try {
            $this->connection = new PDO('mysql:host=' . Database::servername . ';dbname=database', Database::username, Database::password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //$this->createTable();

        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            die(500);
        }
    }

    function getConn(): ?PDO
    {
        return $this->connection;
    }
/*
    function createTable()
    {
        $sql = "
    CREATE TABLE IF NOT EXISTS fieldset(
    id INT AUTO_INCREMENT ,
    type_name varchar(30) NOT NULL UNIQUE,
    description varchar(100) NOT NULL,
  	output_format varchar(100) NOT NULL,
        PRIMARY KEY (id)
    );
   
CREATE TABLE IF NOT EXISTS item(
    id INT AUTO_INCREMENT ,
    sku varchar(30) NOT NULL UNIQUE,
    name varchar(30) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (id));
CREATE TABLE IF NOT EXISTS field(
    id INT AUTO_INCREMENT,
    fieldset_id INT,
  	label varchar(30),
    unit varchar(30),
    PRIMARY KEY (id),
  	FOREIGN KEY (fieldset_id) REFERENCES fieldset(id) ON DELETE CASCADE
    );
CREATE TABLE IF NOT EXISTS field_value(
    id INT AUTO_INCREMENT PRIMARY KEY,
    field_id INT,
  	value int,
  	item_id int,
  	FOREIGN KEY (item_id) REFERENCES item(id) ON DELETE CASCADE,
    FOREIGN KEY (field_id) REFERENCES field(id) ON DELETE CASCADE);


";
        $this->connection->exec($sql);

    }*/
}

/*INSERT INTO item(sku,name, price)
    VALUES
    ('JVC200123', 'some item', 12.44),
    ('JVC20012as3', 's', 1.44),
    ('JVC0123', 'stuff', 32.44);

insert into fieldset(type_name ,description ,output_format) VALUES
('dvd', 'Please provide size of the disc in MB', 'Size: %d MB'),
('book', 'Please provide weight of the book', 'Weight: %d KG'),
('furniture', 'Please provide dimensions in HxWxL format', 'Dimensions: %dx%dx%d CM');

insert into field(fieldset_id,label,unit) values
(1, 'Size','MB' ),
(2, 'Weight','KG' ),
(3, 'Height','CM' ),
(3, 'Width','CM'),
(3, 'Length','CM');

insert into field_value(field_id, value, item_id) values
(1, 300, 1),
(2, 500, 2),
(3, 10, 3),
(4, 50, 3),
(5, 60,3);

*/


