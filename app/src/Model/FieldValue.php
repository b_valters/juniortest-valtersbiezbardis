<?php

namespace App\Model;

class FieldValue
{
    public $field_id;
    public $value;
    public $item_id;

    public function save()
    {
        global $database;
        try {
            $stmt = $database->getConn()->prepare(
                "INSERT INTO field_value(field_id,value, item_id)
                    VALUES
                    ($this->field_id, $this->value, $this->item_id);");
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Insert failed' . $e->getMessage();
        }

    }

}