<?php

namespace App\Model;

use PDO;

class Item
{
    private static $DATA = [];
    public $id;
    public $sku;
    public $name;
    public $price;
    public $output_format;
    public array $fields = [];

    public static function delete(string $data)
    {
        global $database;
        $stmt = $database->getConn()->prepare("DELETE FROM item WHERE id IN ($data)");
        $stmt->execute();
    }



    public static function all(): array
    {
        global $database;

        // In addition to getting the fields of all items,
        // we need to get the appropriate output format for the FieldValues of the item.
        // To do this we have to join the item table with the fieldset table
        // and get the corresponding output format for that item.
        // If we don't group by the item id and the output format, we'll get
        // duplicate items, since an item has many field_values.
        $stmt = $database->getConn()->prepare("SELECT i.id, i.sku, i.name, i.price, fs.output_format
                                                     FROM item as i
                                                     JOIN field_value as fv on fv.item_id = i.id
                                                     JOIN field as f on fv.field_id = f.id
                                                     JOIN fieldset as fs on f.fieldset_id = fs.id
                                                     GROUP BY i.id, fs.output_format;
                                                     ");
        $stmt->execute();
        self::$DATA = $stmt->fetchAll(PDO::FETCH_CLASS, "App\Model\Item");
        return self::$DATA;
    }

    // Returns a string of formatted FieldValues
    public function printFormatted(): string
    {
        $this->getFieldValues();
        $formatted = [];
        foreach ($this->fields as $field) {
            $formatted[] = $field->value;
        }

        return vsprintf($this->output_format, $formatted);
    }

    // Fetches the fields of the item and returns an array of instances of the class FieldValue
    public function getFieldValues(): array
    {
        global $database;
        $stmt = $database->getConn()->prepare("SELECT fv.value 
                                                     FROM item as i
                                                     JOIN field_value as fv on i.id = fv.item_id
                                                     WHERE i.id = $this->id");
        $stmt->execute();
        $this->fields = $stmt->fetchAll(PDO::FETCH_CLASS, "App\Model\FieldValue");
        return $this->fields;
    }

    public function save(): string
    {
        global $database;
        $stmt = $database->getConn()->prepare(
            "INSERT INTO item(sku,name, price)
                    VALUES
                    ('$this->sku', '$this->name', $this->price);");
        $stmt->execute();
        return $database->getConn()->lastInsertId();

    }
}
