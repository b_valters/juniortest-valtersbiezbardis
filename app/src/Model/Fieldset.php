<?php

namespace App\Model;

use PDO;

class Fieldset
{
    private static $DATA = [];
    public $id;
    public array $fields = [];
    public static function all(): array
    {
        global $database;
        $stmt = $database->getConn()->prepare("SELECT *
                                                     FROM fieldset 
                                                     ");
        $stmt->execute();
        self::$DATA = $stmt->fetchAll(PDO::FETCH_CLASS, "App\Model\Fieldset");
        return self::$DATA;
    }
    // to get all the labels and units for the fieldset
    public function getFields()
    {
        global $database;
        $stmt = $database->getConn()->prepare("SELECT field.*
                                                     FROM fieldset
                                                     JOIN field on fieldset.id = field.fieldset_id
                                                     WHERE fieldset.id = $this->id");
        $stmt->execute();
        $this->fields = $stmt->fetchAll(PDO::FETCH_CLASS, "App\Model\Field");
        return $this->fields;
    }

}